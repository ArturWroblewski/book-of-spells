#!/bin/bash

#This script is mostly for creating learning environments on CentOS 8 and Red Hat Enterprise Linux 8.

# This file is structured in four parts:
# I) Install Docker Engine
# II) Verify Docker version
# III) Install Docker Compose
# IV) Verify Docker Compose version

dnf config-manager --add-repo=https://download.docker.com/linux/centos/docker-ce.repo
dnf install docker-ce -y --allowerasing

systemctl start docker
systemctl enable docker

#systemctl status docker
docker version

dnf install -y curl
curl -L "https://github.com/docker/compose/releases/download/v2.16.0/docker-compose-linux-x86_64" -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose

docker-compose --version
