#!/bin/bash

#This script is mostly for creating learning environments on CentOS 8 and Red Hat Enterprise Linux 8.

# This file is structured in four parts:
# I) Install and configure the necessary dependencies
# II) Add the GitLab package repository and install the package
# III) Browse to the hostname and login
# 

############################################################
#                                                          #
#     Install and configure the necessary dependencies     #
#                                                          #
############################################################

sudo dnf install -y curl policycoreutils openssh-server perl
# Enable OpenSSH server daemon if not enabled: sudo systemctl status sshd
sudo systemctl enable sshd
sudo systemctl start sshd
# Check if opening the firewall is needed with: sudo systemctl status firewalld
sudo firewall-cmd --permanent --add-service=http
sudo firewall-cmd --permanent --add-service=https
sudo systemctl reload firewalld

sudo dnf install postfix
sudo systemctl enable postfix
sudo systemctl start postfix

#####################################################################
#                                                                   #
#     Add the GitLab package repository and install the package     #
#                                                                   #
#####################################################################

curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.rpm.sh | sudo bash

#sudo EXTERNAL_URL="https://gitlab.example.com" dnf install -y gitlab-ee

MY_LOCAL_IP=$(ip addr show eth0 | grep 'inet ' | awk '{print $2}' | cut -f1 -d'/')
echo "${MY_LOCAL_IP}"
sudo EXTERNAL_URL="http://${MY_LOCAL_IP}" dnf install -y gitlab-ee

############################################
#                                          #
#     Browse to the hostname and login     #
#                                          #
############################################

# Guidelines and login details
# cat /etc/gitlab/initial_root_password

MY_LOCAL_GITLAB_PASSWORD=$(sudo grep Password: /etc/gitlab/initial_root_password)
echo " "
echo "Log in to the address http://${MY_LOCAL_IP}"
echo "login: root"
echo "${MY_LOCAL_GITLAB_PASSWORD}"
