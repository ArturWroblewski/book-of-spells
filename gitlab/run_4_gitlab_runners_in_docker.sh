#!/bin/bash

#This script is mostly for creating learning environments on CentOS 8 and Red Hat Enterprise Linux 8.

# How to use:
# run_4_gitlab_runners_in_docker.sh GR13489417sfG3sS7v17UxeBFwK6R https://gitlab.com/

# This file is structured in four parts:
# I) Set input variables
# II) Use local system volume mounts to start the Runner ONE container
# III)  Register a Runner ONE using a Docker container
# IV) Verify Docker Compose version


# Set input variables
MY_GITLAB_ADDR="https://gitlab.com/"
MY_REGISTRATION_TOKEN="GR13489417sfG3sS7v17UxeBFwK6R"


# Replace variables if something is given
if [ $# -eq 0 ]
  then
    echo "No arguments supplied - I'm using a token from a file"
else
    MY_REGISTRATION_TOKEN=$1
fi

if [ -z "$2" ]
  then
    echo "No argument supplied - I'm using a address from a file"
else
    MY_GITLAB_ADDR=$2
fi


# Create and configure runner-one nn linux by command line

docker run --cpus=6 -d --name gitlab-runner-one --restart always -v /srv/gitlab-runners/gitlab-runner-one/config:/etc/gitlab-runner -v /var/run/docker.sock:/var/run/docker.sock gitlab/gitlab-runner:latest

docker exec gitlab-runner-one bash -c "gitlab-runner register --non-interactive --executor "docker" --docker-image alpine:latest --url "https://gitlab.com/" --registration-token "GR13489417sfG3sS7v17UxeBFwK6R" --description "docker-runner-one-5800H" --run-untagged="true" --locked="false""

# Create and configure runner-two nn linux by command line

docker run --cpus=6 -d --name gitlab-runner-two --restart always -v /srv/gitlab-runners/gitlab-runner-two/config:/etc/gitlab-runner -v /var/run/docker.sock:/var/run/docker.sock gitlab/gitlab-runner:latest

docker exec gitlab-runner-two bash -c "gitlab-runner register --non-interactive --executor "docker" --docker-image alpine:latest --url "https://gitlab.com/" --registration-token "GR13489417sfG3sS7v17UxeBFwK6R" --description "docker-runner-two-5800H" --run-untagged="true" --locked="false""

# Create and configure runner-tree nn linux by command line

docker run --cpus=6 -d --name gitlab-runner-tree --restart always -v /srv/gitlab-runners/gitlab-runner-tree/config:/etc/gitlab-runner -v /var/run/docker.sock:/var/run/docker.sock gitlab/gitlab-runner:latest

docker exec gitlab-runner-tree bash -c "gitlab-runner register --non-interactive --executor "docker" --docker-image alpine:latest --url "https://gitlab.com/" --registration-token "GR13489417sfG3sS7v17UxeBFwK6R" --description "docker-runner-tree-5800H" --run-untagged="true" --locked="false""

# Create and configure runner-tree nn linux by command line

docker run --cpus=6 -d --name gitlab-runner-four --restart always -v /srv/gitlab-runners/gitlab-runner-four/config:/etc/gitlab-runner -v /var/run/docker.sock:/var/run/docker.sock gitlab/gitlab-runner:latest

docker exec gitlab-runner-four bash -c "gitlab-runner register --non-interactive --executor "docker" --docker-image alpine:latest --url "https://gitlab.com/" --registration-token "GR13489417sfG3sS7v17UxeBFwK6R" --description "docker-runner-four-5800H" --run-untagged="true" --locked="false""

