# This script is mostly for DevOps. 
# The existence of this script in the CI/CD does not affect the results of the tasks. 
# You can remove it from the CI/CD at any time without consequences for the project.

# The script reveals the hardware of the machine. By comparing the execution time of the script on different machines, 
# you can verify which tasks are better to be assigned to dedicated runners and which to shared runners.
# - Shared runners usually have only one core and older architecture, but better internet connection 1 Gbps, 10 Gbps, and 100 Gbps
# - Dedicated runers have more cores, newer architecture, but worse internet connection, which results in longer download times.
# All shared runers CI/CD jobs run on n1-standard-1 instances with  1CPU, 3,75GB RAM, and 20,5GB of storage (16GB available).

# This file is structured in tree parts:
# I) Check local and public IP
# II) Check the operating system
# III) Check CPU and memory
#

# uncomment the lines below or add them in .gitlab-ci.yml if you are using a pure alpine image 
# apk update --quiet --no-progress
# apk add curl --no-progress

echo " "
echo "Pipeline performed on:"
echo " "

PUBLIC_IP=$(curl --silent ifconfig.me/ip)
echo "Public ip: $PUBLIC_IP"
LOCAL_IP=$(ip addr show eth0 | grep 'inet ' | awk '{print $2}' | cut -f1 -d'/')
echo "Local ip: $LOCAL_IP"

echo " "
grep "NAME=" /etc/os-release
grep "VERSION=" /etc/os-release

echo " "
cat /proc/meminfo | grep "Mem"
grep "model name" /proc/cpuinfo
echo " $(grep "model name" /proc/cpuinfo | wc -l) cores available. CPU-quota not verified, CPU-shares not verified"

# uncomment this line if you want to check disk space.
# df -h 